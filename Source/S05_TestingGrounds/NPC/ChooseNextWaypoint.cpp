// Fill out your copyright notice in the Description page of Project Settings.

#include "S05_TestingGrounds.h"
#include "ChooseNextWaypoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "PatrolRouteComponent.h"

EBTNodeResult::Type UChooseNextWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto BlackboardComp = OwnerComp.GetBlackboardComponent();
	auto Index = BlackboardComp->GetValueAsInt(IndexKey.SelectedKeyName);

	// Get Patrol Points
	auto AIController = OwnerComp.GetAIOwner();
	auto PatrolComponent = AIController->GetPawn()->FindComponentByClass<UPatrolRouteComponent>();
	if (ensure(PatrolComponent))
	{
		auto PatrolPoints = PatrolComponent->GetPatrolPoints();

		// Set Next Waypoint
		if (PatrolPoints.Num() > 0)
		{
			BlackboardComp->SetValueAsObject(WaypointKey.SelectedKeyName, PatrolPoints[Index]);

			// Cycle Index
			Index++;
			if (Index == PatrolPoints.Num())
			{
				Index = 0;
			}
			BlackboardComp->SetValueAsInt(IndexKey.SelectedKeyName, Index);
		}
	}

	return EBTNodeResult::Succeeded;
}
